var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(function (req, res, next) {
res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
next();
});

var requestjson =require('request-json');
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca4mb46795/collections/movimientos?apiKey=fEnKK1XabNAEWipbnsOXq1SEmbEuCptR";
var clientedeMlab = requestjson.createClient(urlmovimientosMlab);

//---------------------
var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function (req, res){
res.sendfile(path.join(__dirname, 'index.html'));

});

app.get("/clientes/:idCliente",function (req, res){
res.send("Aqui tiene su numero de cliente: " +req.params.idCliente);

});

app.post("/", function (req, res){
res.send("Hemos recibido su peticion cambiada");
});

app.put("/", function (req, res){
res.send("Hemos recibido su peticion pull");
});

app.delete("/", function (req, res){
res.send("Hemos recibido su peticion delete");

});
//----------get movimientos-------------------
app.get("/movimientos",function (req, res){
  clientedeMlab.get('',function(err,resM,body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }

  });
});

//------------POST idcliente, nombre, apellido
app.post("/movimientos", function (req, res){
  clientedeMlab.post('',req.body, function(err,resM,body){
    res.send(body);
  });
});
